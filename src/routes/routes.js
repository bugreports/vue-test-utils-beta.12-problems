import CategoryIndex from '@cmps/CategoryIndex';
import CategoryForm from '@cmps/CategoryForm';
import StatusIndex from '@cmps/StatusIndex';
import StatusForm from '@cmps/StatusForm';
import PersonIndex from '@cmps/PersonIndex';
import PersonForm from '@cmps/PersonForm';
import AddressIndex from '@cmps/AddressIndex';

const routes = [
  {
    path: '/categories',
    name: 'categories',
    component: CategoryIndex,
  },
  {
    path: '/categories/new',
    name: 'category-new',
    component: CategoryForm,
  },
  {
    path: '/categories/:category_id/edit',
    name: 'category-edit',
    component: CategoryForm,
  },
  {
    path: '/statuses',
    name: 'statuses',
    component: StatusIndex,
  },
  {
    path: '/statuses/new',
    name: 'status-new',
    component: StatusForm,
  },
  {
    path: '/statuses/:status_id/edit',
    name: 'status-edit',
    component: StatusForm,
  },
  {
    path: '/people',
    name: 'people',
    component: PersonIndex,
  },
  {
    path: '/people/new',
    name: 'person-new',
    component: PersonForm,
  },
  {
    path: '/people/:person_id/edit',
    name: 'person-edit',
    component: PersonForm,
  },
  {
    path: '/addresses',
    name: 'addresses',
    component: AddressIndex,
  },
];

export {
  routes,
};

