const defaultState = {
  sysmsg: {
    display: false,
    heading: 'Notificação do Sistema',
    text: '',
  },

  alert: {
    display: false,
    type: 'info',
    text: '',
  },
};


const store = {
  state: {
    sysmsg: {
      display: defaultState.sysmsg.display,
      heading: defaultState.sysmsg.heading,
      text: defaultState.sysmsg.text,
    },

    alert: {
      display: defaultState.alert.display,
      type: defaultState.alert.type, // 'success', 'info', 'warning', 'error'
      text: defaultState.alert.text,
    },
  },

  resetState () {
    this.state.sysmsg = JSON.parse(JSON.stringify(defaultState.sysmsg));
    this.state.alert = JSON.parse(JSON.stringify(defaultState.alert));
  },

  /**
   * @param Bool newValue
   */
  setSysmsgDisplayAction (newValue) {
    this.state.sysmsg.display = newValue;
  },

  /**
   * @param String newHeading
   */
  setSysmsgHeadingAction (newHeading) {
    this.state.sysmsg.heading = newHeading;
  },

  clearSysmsgHeadingAction () {
    this.state.sysmsg.heading = '';
  },

  /**
   * @param String newText
   */
  setSysmsgTextAction (newText) {
    this.state.sysmsg.text = newText;
  },

  clearSysmsgTextAction () {
    this.state.sysmsg.text = '';
  },

  /**
   * Displays a system message dialog with given data.
   *
   * App.vue reacts to this store change and displays
   * the dialog with provided config data.
   *
   * @param Object data
   *   { heading: String, text: String }
   */
  displaySysmsgAction(data) {
    if (data.heading) {
      this.setSysmsgHeadingAction(data.heading);
    }
    if (data.text) {
      this.setSysmsgTextAction(data.text);
    }
    this.setSysmsgDisplayAction(true);
  },

  /**
   * @param Bool newValue
   */
  setAlertDisplayAction (newValue) {
    this.state.alert.display = newValue;
  },

  /**
   * @param String newType
   *  Possible values: 'success', 'info', 'warning', 'error'
   */
  setAlertTypeAction (newType) {
    this.state.alert.type = newType;
  },

  /**
   * @param String newText
   */
  setAlertTextAction (newText) {
    this.state.alert.text = newText;
  },

  /**
   * Displays a system alert with given dta.
   *
   * App.vue reacts to this store change and displays
   * the alert with provided config data.
   *
   * @param Object data
   *  { type: String, text: String }
   */
  displayAlertAction (data) {
    if (data.type) {
      this.setAlertTypeAction(data.type);
    }

    if (data.text) {
      this.setAlertTextAction(data.text);
    }

    this.setAlertDisplayAction(true);
  },
};

export { store };

