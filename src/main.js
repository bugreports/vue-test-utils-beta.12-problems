import Vue from 'vue';
import App from './App.vue';
import Vuetify from 'vuetify';
import axios from 'axios';
import { router } from './routes/router';

import 'vuetify/dist/vuetify.css';
import './assets/imgs/favicon.png';

// `VBSERP' is set on the spa template with laravel.
axios.defaults.baseURL = VBSERP.baseURL;
axios.defaults.headers.common = {
  'X-Requested-With': 'XMLHttpRequest',
  'X-CSRF-TOKEN': VBSERP.csrfToken
};

//axios.interceptors.response.use(res => {
//  return res;
//}, err => {
//  if (401 === err.response.status && 'Unauthorized'.match(err.response.statusText)) {
//    EvtBus.$emit('dialog-sysmsg', {
//      display: true,
//      message: `
//        Parece que a sessão expirou. Faça o
//        <a href='/login'>login</a>
//        novamente.`,
//    });
//  }
//  return Promise.reject(err);
//});

Vue.config.productionTip = false;

Vue.use(Vuetify);

new Vue({
  el: '#vueroot',
  router,
  render: h => h(App)
});


