const status1 = {
  id: 1,
  name: 'Active Status',
};

const status2 = {
  id: 2,
  name: 'Inactive Status',
};

const status3 = {
  id: 3,
  name: 'Disabled Status',
};

const status4 = {
  id: 4,
  name: 'Premium Status',
};

export {
  status1,
  status2,
  status3,
  status4,
};
