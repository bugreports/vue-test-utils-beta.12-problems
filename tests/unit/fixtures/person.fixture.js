const person1 = {
  id: 1,
  name: 'Foo Person',
  email: 'foo@foo.net',
  landline_phone: '54 0001 0002',
  cell_phone: '54 9 9991 9992',
  union_code: 'UNION_CODE_1',
  observation: 'person one observation',
  account_id: 1,
  status_id: 1,
};

const person2 = {
  id: 1,
  name: 'Bar Person',
  email: 'bar@bar.net',
  landline_phone: '55 0001 0002',
  cell_phone: '55 9 9991 9992',
  union_code: 'UNION_CODE_2',
  observation: 'person two observation',
  account_id: 1,
  status_id: 2,
};

export {
  person1,
  person2,
};
