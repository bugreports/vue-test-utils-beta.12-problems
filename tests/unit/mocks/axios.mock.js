const mockAxios = {
  get: jest.fn().mockImplementation(() => {
    return new Promise((res, rej) => {
      mockAxios._resolve = res;
      mockAxios._reject = rej;
    });
  }),

  post: jest.fn().mockImplementation(() => {
    return new Promise((res, rej) => {
      mockAxios._resolve = res;
      mockAxios._reject = rej;
    });
  }),

  patch: jest.fn().mockImplementation(() => {
    return new Promise((res, rej) => {
      mockAxios._resolve = res;
      mockAxios._reject = rej;
    });
  }),

  /**
   * @param {Object} resp
   *  { status: Number, data: Object }
   */
  mockSuccess (resp) {
    this._resolve(resp);
  },

  /**
   * @param {Object} resp
   *  { status: Number, data: Object }
   */
  mockError (resp) {
    this._reject(resp);
  },

  _resolve: null,
  _reject: null
};

//const mockAxios = {
//  post: () => new Promise(rej => {
//    reject({status: 503});
//  }),
//  get: () => new Promise(res => {
//    resolve({ status: 200 });
//  }),
//};

export default mockAxios;

