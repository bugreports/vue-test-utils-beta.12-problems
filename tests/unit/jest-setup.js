//
// package.json has a section that instructs jest to
// read this setup file.
//
import Vue from 'vue';

// Because of the regeneratorRuntime error.
import 'babel-polyfill';

// DON'T DO THIS HERE
// Vue.use(Vuetify);

Vue.config.productionTip = false;


global.info = console.info.bind(console);
global.log = console.log.bind(console);
global.error = console.error.bind(console);

/**
 * Resolves all pending promises so tests become easier:
 * https://github.com/facebook/jest/issues/2157#issuecomment-279171856
 * There is also: https://www.npmjs.com/package/flush-promises
 *
 * @return {Promise}
 */
global.flushPromises = function flushPromises() {
 // setImmediate is not even on standards track, according to MDN.
 //return new Promise(resolve => setImmediate(resolve));
 return new Promise(function(resolve) {
   scheduler(resolve);
 });
};

/**
 * Adds a warapping `div data-app="true"` to the body so that we don't
 * get Vuetify complaining about missing data-app attribute for some components.
 *
 * @return undefined
 */
global.addElemWithDataAppToBody = function addElemWithDataAppToBody() {
  const app = document.createElement('div');
  app.setAttribute('data-app', true);
  document.body.append(app);
};


/**
 * Create a component in an "editing thing" status.
 *
 * @param id: number
 * @param routes: array<Object>
 * @pram Component: VueComponent
 * @return VueTestUtilsWrapper
 */
global.createEditingComponentForm = function createEditingComponentForm(id, routes, component) {
  const lvue = createLocalVue();

  lvue.use(VueRouter);
  lvue.use(Vuetify);

  const router = new VueRouter({ routes });

  router.push({ name: 'status-edit', params: { status_id: id }});

  const wrp = mount(component, {
    localVue: lvue,
    router,
  });

  return wrp;
};


/**
 * Create a component in a "creating thing" status.
 *
 * @param routes: array<Object>
 * @pram Component: VueComponent
 * @return VueTestUtilsWrapper
 */
global.createEditingComponentForm = function createEditingComponentForm(routes, component) {
  const lvue = createLocalVue();

  lvue.use(VueRouter);
  lvue.use(Vuetify);

  const router = new VueRouter({ routes });

  router.push({ name: 'status-new' });

  const wrp = mount(component, {
    localVue: lvue,
    router,
  });

  return wrp;
};


/**
 * Types characters into a form's input field and triggers `input' event.
 *
 * @param Wrapper wrapper (from Vue-Test-Utils)
 *  The selector will be used in the context of this wrapper.
 *
 * @param CSSSelector|Wrapper selector
 *  A valid selector like 'div.foobar' or "input[name='person[email]']"
 *  or an instance of Wrapper so it is already the element proper.
 *
 * @param String value
 *  The param to be inserted into the input field.
 *
 * @return HTMLElement
 *  The html element found on the wrapper using the selector.
 */
global.typeIn = function typeIn(wrapper, selector, value) {
  let field;

  if (typeof selector === 'string') {
    field = wrapper.find(selector);
  }
  else { // Assume it is a string/css selector ready to use.
    field = selector;
  }

  field.element.value = value;

  field.trigger('input');

  return field;
};

