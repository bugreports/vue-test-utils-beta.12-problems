
/**
 * Selects one or more items from the combobox.
 *
 * <v-select> accepts a 'content-class' property to specify the
 * name of the class to be used for the combobox that <v-select>
 * creates and opens. That combobox can be accessed through
 * the wrapper then, even though it is created outside the
 * <v-select> component itself.
 *
 * This function does not check that the element exists before
 * attempting to click on it. Make sure your component has
 * that amount of items to be in th combobox according to
 * what you want to click/select.
 *
 * Basically, the combobox renders as:
 *
 *  div » ul » li » a
 *
 * If we “click” on the `li' or `a', we select that item and if the
 * component useing v-select correctly sets things up, we should
 * have the selected categories ids on some data property.
 *
 * @param {Wrapper} wrp
 *  A vue-test-utils Wrapper object.
 *
 * @param {Wrapper} wrpSelect
 *  A wrapper object which is the <select> equivalent on the page.
 *  We use this to trigger focus and to trigger input to make
 *  selections actually apply.
 *
 * @param {Selector} comboboxSelector
 *  A valid css selector for the pupup with the list to select from.
 *
 * @param {Array<Number>} indexes
 *  An array of indexes to select.
 */
const selectItemsFromCombobox = function selectItemsFromCombobox(wrp, wrpSelect, comboboxSelector, indexes) {

  // Select
  const rows = wrp.findAll(comboboxSelector);

  indexes.forEach(index => {
    // We always have to focus on it again, it seems.
    // Unlike on the real UI, we have to open it again, it seems, or the
    // ids are overriten instead of appended to selectedCategoriesIds.
    wrpSelect.trigger('focus');
    rows.at(index).trigger('click');
  });

  wrpSelect.trigger('input');
};

export {
  selectItemsFromCombobox,
};

