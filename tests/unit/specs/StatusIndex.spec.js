import { shallow, mount, createLocalVue } from '@vue/test-utils';
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import StatusIndex from '@cmps/StatusIndex.vue';
import StatusForm from '@cmps/StatusForm.vue';
import axios from 'axios';
import { store } from '@/store/store.js';

import { status1, status2 } from '@fixtures/status.fixture.js';

// Jest mocks are hoisted to the top of the file no mater
// where you put them.
// https://facebook.github.io/jest/docs/en/manual-mocks.html#using-with-es-module-imports
jest.mock('axios', () => ({
  // path is what comes from the parameter to axios.get('/some-path').
  get: jest.fn((path) => {
    if ('/statuses' === path) {
      // Can't use `import` here and jest mocks cannot access variables
      // imported above, outside jest.mock() scope.
      const statuses = require('@fixtures/status.fixture.js');
      return Promise.resolve({
        status: 200,
        statusText: 'OK',
        data: [statuses.status1, statuses.status2],
      });
    }
    else if ('/statuses-404' === path) {
      return Promise.reject({
        status: 404,
        data: { error: 'Not Found: 404' }
      });
    }
    else {
      return Promise.reject({
        status: 500,
        data: { error: 'Internal Server Error: 500' }});
    }
  })
}));

describe('StatusIndex.spec.js', function () {
  let wrp;

  const routes = [
    { path: '/statuses', name: 'statuses' },
    { path: '/statuses/new', name: 'status-new' },
    { path: '/statuses/:status_id/edit', name: 'status-edit' }
  ];

  const router = new VueRouter({routes});

  beforeEach(() => {

    const localVue = createLocalVue();
    localVue.use(VueRouter);
    localVue.use(Vuetify);

    router.push({ name: 'statuses' });

    wrp = mount(StatusIndex, {
      localVue: localVue,
      router,
    });
  });

  describe('Basic Structure', () => {

    it('has a mounted hook', () => {
      expect(typeof StatusIndex.mounted).toBe('function');
    });

    it('has correct route', () => {
      expect(wrp.vm.$route.name).toEqual('statuses');
    });

    it('has container with proper <entity> and <entity>-index css classes', () => {
      expect(wrp.classes()).toContain('status');
      expect(wrp.classes()).toContain('status-index');
    });

    it('has an h2 tag with proper class and text', () => {
      expect(wrp.find('h2.crud-title').text()).toEqual('Status - Listagem');
    });

    it('has add btn', () => {
      expect(wrp.find('.btn-add-status').exists()).toBe(true);
    });

    it('always displays .index-of-things', () => {
      // Displays it no matter whether or not we have people to display.
      wrp.setData({ statuses: [] });
      expect(wrp.contains('.index-of-things')).toBe(true);

      wrp.setData({ statuses: [status1, status2]});
      expect(wrp.contains('.index-of-things')).toBe(true);
    });

    it('conditionally displays container <entity>-index-row', () => {
      // If statuses array is empty, the v-for has nothing to iterate
      // over and we therefore render no rows on the UI.
      wrp.setData({ statuses: []});
      expect(wrp.contains('.status-index-row')).toBe(false);

      // But if we have statuses to show, we then should see the exact
      // number of rows.
      // @TODO: (ignoring pagination for this stage of the project).
      wrp.setData({ statuses: [status1, status2] });
      expect(wrp.findAll('.status-index-row').length).toEqual(2);
    });

    it('each <entity>-index-row has an edit button', () => {
      wrp.setData({ people: [status1, status2] });
      const rows = wrp.findAll('.status-index-row');
      // <v-btn> renders as <a href="..."> because of :to property.
      expect(rows.contains('a.btn-edit')).toBe(true);
    });
  });


  describe('Fetch/Persist Data', () => {
    it('fetches and renders some <entities>', async () => {
      expect.assertions(2);
      await wrp.vm.fetchStatuses();
      wrp.update();
      const rows = wrp.findAll('.index-of-things .status-index-row');
      expect(rows.at(0).text()).toContain('Active Status');
      expect(rows.at(1).text()).toContain('Inactive Status');
    });

    it('handles requests to <entities> index 404 url', async () => {
      await wrp.vm.fetchStatuses('/statuses-404').catch(err => {
        expect(err.status).toBe(404);
        expect(err.data.error).toBe('Not Found: 404');
        store.displayAlertAction({
          type: 'warning',
          text: 'Erro a listar status.',
        });

        expect(store.state.alert.type).toEqual('warning');
        expect(store.state.alert.text).toEqual('Erro a listar status.');
        expect(store.state.alert.display).toBe(true);
      });
    });

    it('invokes fetch<entities> when component is created', () => {
      const fetchStatuses = jest.fn();
      const lwrp = shallow(StatusIndex, {
        methods: { fetchStatuses }
      });
      expect(fetchStatuses).toHaveBeenCalled();
    });

    it('invokes fetch<entities> to a 500 sorts of serve response', async () => {
      expect.assertions(1);
      return wrp.vm.fetchStatuses('/no-no-no')
        .catch(err => {
          expect(err.data.error).toEqual('Internal Server Error: 500');
        });
    });
  }); // Fetch/Persist Data

});
