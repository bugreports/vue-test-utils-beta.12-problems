import { shallow, mount, createLocalVue } from '@vue/test-utils';
import flushPromises from 'flush-promises';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import PersonForm from '@cmps/PersonForm.vue';
import axios from 'axios';
import { store } from '@/store/store';
import { routes } from '@/routes/routes';

import { person1, person2 } from '@fixtures/person.fixture.js';
import { category1, category2, category3, category4 } from '@fixtures/category.fixture.js';
import { status1, status2, status3, status4 } from '@fixtures/status.fixture.js';

import * as H_ from '../helpers';

jest.mock('axios', () => require('../mocks/axios.mock.js'));

// Fields that are present in the DB that should definitely
// be in the form. Commented elements may be in the database
// but are not requried or updated/inserted through the UI.
const personProperties = [
  // 'id',
  { prop: 'name', tag: 'input', type: 'text', label: 'Nome' },
  { prop: 'email', tag: 'input', type: 'email', label: 'E-mail' },
  { prop: 'landline_phone', tag: 'input', type: 'text', label: 'Telefone Fixo' },
  { prop: 'cell_phone', tag: 'input', type: 'text', label: 'Telefone Celular' },
  { prop: 'union_code', tag: 'input', type: 'text', label: 'Código de Registro (CRM, CRC, etc.)' },
  // 'observation',
  // 'created_at',
  // 'updated_at',
  // 'account_id',
  { prop: 'status', tag: 'v-select', type: 'vuetify thing', label: 'Selecionar Status' },
  { prop: 'categories', tag: 'v-select', type: 'vuetify thing', label: 'Selecionar Categoria(s)' },
]

/**
 * Fill in PersonForm with dummy data.
 *
 * Can be used for both create and edit forms.
 *
 * @param {Wrapper} wrp
 */
function fillPersonForm(wrp) {
  const selectCategories = wrp.find('.wrp-person-categories');
  const selectStatus = wrp.find('.wrp-person-status');
  typeIn(wrp, '.wrp-person-name input', 'Some Category');
  typeIn(wrp, '.wrp-person-email input', 'email@email.net');
  typeIn(wrp, '.wrp-person-landline_phone input', '54 0001 0001');
  typeIn(wrp, '.wrp-person-cell_phone input', '54 9 9991 9991');
  typeIn(wrp, '.wrp-person-union_code input', 'SOME_UNION_CODE');
  H_.selectItemsFromCombobox(wrp, selectCategories, '.categories-combobox li', [1, 3]);
  H_.selectItemsFromCombobox(wrp, selectStatus, '.statuses-combobox li', [2]);
};

// Avoid vuetify problems with [app-data] missing thing.
addElemWithDataAppToBody();

describe('PersonForm.spec.js', () => {

  let wrpNew;
  let wrpEdit;

  beforeEach(() => {

    const routerNew = new VueRouter({ mode: 'abstract', routes });
    const routerEdit = new VueRouter({ mode: 'abstract', routes });

    const localVueNew = createLocalVue();
    const localVueEdit = createLocalVue();

    localVueNew.use(VueRouter);
    localVueNew.use(Vuetify);

    localVueEdit.use(VueRouter);
    localVueEdit.use(Vuetify);

    routerNew.push({ name: 'person-new' });
    routerEdit.push({ name: 'person-edit', params: { person_id: 1 }});

    wrpNew = mount(PersonForm, {
      localVue: localVueNew,
      router: routerNew,
    });

    wrpEdit = mount(PersonForm, {
      localVue: localVueEdit,
      router: routerEdit,
    });

  });

  describe('Default Structure', () => {
    it('has container with proper <entity> and <entity>-form css classes', () => {
      expect(wrpNew.contains('.person.person-form')).toBe(true);
      expect(wrpEdit.contains('.person.person-form')).toBe(true);
    });

    it('has ‘isEditing’ true or false', () => {
      expect(wrpNew.vm.isEditing).toBe(false);
      expect(wrpEdit.vm.isEditing).toBe(true);
    });

    it('has ‘h2.crud-title’ with proper create/edit text', () => {
      expect(wrpNew.contains('h2.crud-title')).toBe(true);
      expect(wrpEdit.contains('h2.crud-title')).toBe(true);
      expect(wrpNew.find('h2.crud-title').text()).toEqual('Pessoas - Adicionar');
      expect(wrpEdit.find('h2.crud-title').text()).toEqual('Pessoas - Editar');
    });

    it('has a ‘form’ for the new/editing <entity>', () => {
      expect(wrpNew.contains('.person-form form')).toBe(true);
      expect(wrpEdit.contains('.person-form form')).toBe(true);
    });

    it('has cancel and submit buttons', () => {
      expect(wrpNew.contains('.person-form .btn-cancel')).toBe(true);
      expect(wrpNew.contains('.person-form .btn-submit')).toBe(true);
      expect(wrpEdit.contains('.person-form .btn-cancel')).toBe(true);
      expect(wrpEdit.contains('.person-form .btn-submit')).toBe(true);
    });

    it('has container with proper class plus label for each field', () => {
      // const elemNew = wrpNew.find('.wrp-status-name');
      // const elemEdit = wrpEdit.find('.wrp-status-name');

      personProperties.forEach(obj => {
        // Contains container with class resembling property in question
        expect(wrpNew.contains(`.wrp-person-${obj.prop}`)).toBe(true);
        expect(wrpNew.find(`.wrp-person-${obj.prop} label`).text()).toEqual(obj.label);
      });

    });

  }); // Default Structure

  describe('Form - Create/Edit', () => {

    /**
     * <v-select> does not use <select>, but rather a combination
     * of <div>, <ul>, <li>, <a>, <label> etc. So, let's make a
     * very specific test about selecting and unselecting categories
     * which allows selecting multiple items and uses chips.
     */
    it('multi-selects categories from the combobox popup', () => {
      wrpNew.setData({
        categories: [category1, category2, category3, category4],
        selectedCategoriesIds: [],
      });

      const selectElem = wrpNew.find('.wrp-person-categories');

      // No categories are selected thus far.
      expect(wrpNew.vm.selectedCategoriesIds).toEqual([]);

      // Open the select and click on the first item.
      selectElem.trigger('focus');
      // Select
      wrpNew.findAll('.categories-combobox li').at(0).trigger('click');
      expect(wrpNew.vm.selectedCategoriesIds).toEqual([1]);

      // Unlike on the real UI, we have to open it again, it seems, or the
      // ids are overriten instead of appended to selectedCategoriesIds.
      selectElem.trigger('focus');
      // Select
      wrpNew.findAll('.categories-combobox li').at(2).trigger('click');
      expect(wrpNew.vm.selectedCategoriesIds).toEqual([1, 3]);

      selectElem.trigger('focus');
      // Unselect by clicking on the popup list, not on the ✖ icon of the chip.
      wrpNew.findAll('.categories-combobox li').at(0).trigger('click');
      expect(wrpNew.vm.selectedCategoriesIds).toEqual([3]);

      selectElem.trigger('focus');
      // Select
      wrpNew.findAll('.categories-combobox li').at(1).trigger('click');
      expect(wrpNew.vm.selectedCategoriesIds).toEqual([3, 2]); // Order matters.

      selectElem.trigger('focus');
      // Select the first again.
      wrpNew.findAll('.categories-combobox li').at(0).trigger('click');
      expect(wrpNew.vm.selectedCategoriesIds).toEqual([3, 2, 1]); // Order matters.
    });

    // This was problematic.
    it('removes selected categories by clicking on the chips ✖ icon', async () => {
      // An edit form in which some categories are already present/selected.
      wrpEdit.setData({
        categories: [category1, category2, category3, category4],
        selectedCategoriesIds: [1, 3, 4],
      });

      // setData does not seem to also readily update vuetify components
      // so we use nextTick() or update() it seems.
      await wrpEdit.vm.$nextTick();

      const selectElem = wrpEdit.find('.wrp-person-categories');

      expect(wrpEdit.vm.selectedCategoriesIds).toEqual([1, 3, 4]);

      // Remove by clicking on the chip's ✖ icon.
      // Vuetify select chips has 'chip__close' class.
      // We should now have chips indexed 0, 1 and 2.

      selectElem.findAll('.chip__close').at(1).trigger('click');
      selectElem.trigger('input'); // IMPORTANT!
      await wrpEdit.vm.$nextTick();
      expect(wrpEdit.vm.selectedCategoriesIds).toEqual([1, 4]);
    });

    it('selects a status from the combobox popup', async () => {
      wrpNew.setData({
        statuses: [status1, status2, status3, status4],
        selectedStatusId: undefined,
      });

      await wrpNew.vm.$nextTick();
      const selectElem = wrpNew.find('.wrp-person-status');

      selectElem.trigger('click');

      // wrpNew.html() does not have .statuses-combobox, but I can still
      // select it and click on an item. Probably because it is another component.
      selectElem.findAll('.statuses-combobox li').at(2).trigger('click');
      await wrpNew.vm.$nextTick();
      expect(wrpNew.vm.selectedStatusId).toEqual(3);
    });

    it('new <entity> form empty fields', () => {
      expect(wrpNew.find('.wrp-person-name input').element.value).toEqual('');
      expect(wrpNew.find('.wrp-person-email input').element.value).toEqual('');
      expect(wrpNew.find('.wrp-person-landline_phone input').element.value).toEqual('');
      expect(wrpNew.find('.wrp-person-cell_phone input').element.value).toEqual('');
      expect(wrpNew.find('.wrp-person-union_code input').element.value).toEqual('');
      // select becomes an input
      expect(wrpNew.find('.wrp-person-status input').element.value).toEqual('');
      // choosen categories become .chips
      expect(wrpNew.findAll('.wrp-person-status .chip').length).toEqual(0);
    });

    it('edit <entity> form has filled in fields', async () => {
      wrpEdit.setData({
        person: person1,
        statuses: [status1, status2, status3, status4],
        selectedStatusId: 3,
        categories: [category1, category2, category3, category4],
        selectedCategoriesIds: [2, 4],
      });

      await wrpEdit.vm.$nextTick();

      expect(wrpEdit.find('.wrp-person-name input').element.value).toEqual(person1.name);
      expect(wrpEdit.find('.wrp-person-email input').element.value).toEqual(person1.email);
      expect(wrpEdit.find('.wrp-person-landline_phone input').element.value).toEqual(person1.landline_phone);
      expect(wrpEdit.find('.wrp-person-cell_phone input').element.value).toEqual(person1.cell_phone);
      expect(wrpEdit.find('.wrp-person-union_code input').element.value).toEqual(person1.union_code);
      // select becomes divs and spans, and the input doesn't seem to have a value.
      expect(wrpEdit.find('.wrp-person-status').text()).toContain(status3.name);
      // // select multiple categories become .chips
      expect(wrpEdit.findAll('.wrp-person-categories .chip').length).toEqual(2);
      expect(wrpEdit.findAll('.wrp-person-categories .chip').at(0).text()).toContain(category2.name);
      expect(wrpEdit.findAll('.wrp-person-categories .chip').at(1).text()).toContain(category4.name);
    });

    // This was problematic.
    it('cancels the create/edit action', () => {
      const spyNew = jest.fn();
      const spyEdit = jest.fn();
      wrpNew.vm.$router.push = spyNew;
      wrpEdit.vm.$router.push = spyEdit;
      wrpNew.find('.person-form .btn-cancel').trigger('click');
      wrpEdit.find('.person-form .btn-cancel').trigger('click');
      expect(spyNew).toHaveBeenCalledWith({ name: 'people' });
      expect(spyEdit).toHaveBeenCalledWith({ name: 'people' });
    });

    it('submits create form', () => {
      wrpNew.setData({
        categories: [category1, category2, category3, category4],
        statuses: [status1, status2, status3, status4],
      });

      const spy = jest.spyOn(wrpNew.vm, 'requestCreate');
      const btn = wrpNew.find('button.btn-submit');

      fillPersonForm(wrpNew);

      btn.trigger('click');

      expect(spy).toHaveBeenCalled();
    });

    it('submits edit form', () => {
      wrpEdit.setData({
        categories: [category1, category2, category3, category4],
        statuses: [status1, status2, status3, status4],
      });
      const spy = jest.spyOn(wrpEdit.vm, 'requestUpdate');
      const btn = wrpEdit.find('button.btn-submit');

      fillPersonForm(wrpEdit);

      btn.trigger('click');
      expect(spy).toHaveBeenCalled();
    });

    it('redirects to index if create was successful', async () => {
      jest.clearAllMocks();
      jest.resetModules();

      wrpNew.setData({
        categories: [category1, category2, category3, category4],
        statuses: [status1, status2, status3, status4],
      });

      const pushSpy = jest.spyOn(wrpNew.vm.$router, 'push');
      const btn = wrpNew.find('button.btn-submit');

      fillPersonForm(wrpNew);

      btn.trigger('click');

      axios.mockSuccess({
        status: 201,
      });

      await wrpNew.vm.$nextTick();

      expect(pushSpy).toHaveBeenCalledWith({
        name: 'people',
        params: {},
        path: '/people'
      });
    });

    it('redirects to index if update was successful', async () => {
      wrpEdit.setData({
        categories: [category1, category2, category3, category4],
        statuses: [status1, status2, status3, status4],
      });

      const pushSpy = jest.spyOn(wrpEdit.vm.$router, 'push');
      const btn = wrpEdit.find('.btn-submit');

      fillPersonForm(wrpEdit);

      btn.trigger('click');

      axios.mockSuccess({
        status: 200,
      });

      await wrpEdit.vm.$nextTick();

      expect(pushSpy).toHaveBeenCalledWith({
        name: 'people',
        params: {},
        path: '/people'
      });
    });

    it('updates store with alert message when create fails', async () => {

      wrpNew.setData({
        categories: [category1, category2, category3, category4],
        statuses: [status1, status2, status3, status4],
      });

      const btnSubmit = wrpNew.find('.btn-submit');

      const spyAlert = jest.spyOn(store, 'displayAlertAction');

      fillPersonForm(wrpNew);

      btnSubmit.trigger('click');

      axios.mockError({
        status: 500,
        data: {
          error: 'Internal Server Error',
        }
      });

      expect.assertions(1);

      // https://forum.vuejs.org/t/need-to-use-nexttick-twice/28341
      // flush-promises gave unintelligible errors.
      await wrpNew.vm.$nextTick();
      await wrpNew.vm.$nextTick();

      expect(spyAlert).toHaveBeenCalledWith({
        type: 'warning',
        text: 'Erro ao inserir dados da pessoa.'
      });
    });

    it('updates store with alert message when create succeeds', async () => {
      const spyAlert = jest.spyOn(store, 'displayAlertAction');
      const btnSubmit = wrpNew.find('.btn-submit');

      btnSubmit.trigger('click');
      axios.mockSuccess({
        status: 201,
      });

      await wrpNew.vm.$nextTick();
      await wrpNew.vm.$nextTick();
      expect(spyAlert).toHaveBeenCalledWith({
        type: 'success',
        text: 'Dados da pessoa inseridos com sucesso!',
      });
    });

    it('updates store with alert message when update fails', async () => {
      const spyAlert = jest.spyOn(store, 'displayAlertAction');
      const btnSubmit = wrpEdit.find('.btn-submit');

      btnSubmit.trigger('click');
      axios.mockError({
        status: 500,
      });

      await wrpEdit.vm.$nextTick();
      await wrpEdit.vm.$nextTick();
      expect(spyAlert).toHaveBeenCalledWith({
        type: 'warning',
        text: 'Erro ao atualizar dados da pessoa.'
      });
    });

    it('updates store with alert message when update succeeds', async () => {
      const spyAlert = jest.spyOn(store, 'displayAlertAction');
      const btnSubmit = wrpEdit.find('.btn-submit');

      btnSubmit.trigger('click');
      axios.mockSuccess({
        status: 200,
      });

      await wrpEdit.vm.$nextTick();
      await wrpEdit.vm.$nextTick();
      expect(spyAlert).toHaveBeenCalledWith({
        type: 'success',
        text: 'Dados da pessoa atualizados com sucesso!',
      });
    });

  }); // Form - Create/Edit



}); // PersonForm.spec.js



