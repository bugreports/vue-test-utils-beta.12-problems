import { shallow, mount, createLocalVue } from '@vue/test-utils';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import PersonIndex from '@cmps/PersonIndex';
import { store } from '@/store/store';
import axios from 'axios';
import { person1, person2 } from '@fixtures/person.fixture.js';

//jest.mock('axios', () => require('../mocks/axios.mock.js'));


jest.mock('axios', () => ({
  // path is what comes from the parameter to axios.get('/some-path').
  get: jest.fn((path) => {
    // Can't use `import` here and jest mocks cannot access variables
    // imported above, outside jest.mock() scope.
    const people = require('@fixtures/person.fixture.js');
    if ('/people' === path) {
      return Promise.resolve({
        status: 200,
        data: [people.person1, people.person2]
      });
    }
    else if ('/people-404' === path) {
      return Promise.reject({
        status: 404,
        data: { error: 'Not Found: 404' },
      });
    }
    else {
      return Promise.reject({
        status: 500,
        data: { error: 'Internal Server Error: 500' },
      });
    }
  }),
}));

const routes = [
  { path: '/people', name: 'people' },
  { path: '/people/new', name: 'person-new' },
  { path: '/people/:person_id/edit', name: 'person-edit' }
];

const router = new VueRouter({ routes, mode: 'abstract' });

describe('Person.spec.js', () => {
  let wrp;

  beforeEach(() => {
    const lvue = createLocalVue();

    lvue.use(Vuetify);
    lvue.use(VueRouter);

    router.push({ name: 'people' });

    wrp = mount(PersonIndex, {
      localVue: lvue,
      router,
    });
  });

  describe('Basic Structure', () => {

    it('has a mounted hook', () => {
      expect(typeof PersonIndex.mounted).toEqual('function');
    });

    it('has correct route', () => {
      expect(wrp.vm.$route.name).toEqual('people');
    });

    it('has container with proper <entity> and <entity>-index css classes', () => {
      expect(wrp.classes()).toContain('person');
      expect(wrp.classes()).toContain('person-index');
    });

    it('has an h2 tag with proper class and text', () => {
      expect(wrp.find('h2.crud-title').text()).toEqual('Pessoas - Listagem');
    });

    it('has add btn', () => {
      expect(wrp.find('.btn-add-person').exists()).toBe(true);
    });

    it('always displays .index-of-things', () => {
      // Displays it no matter whether or not we have people to display.
      wrp.setData({ people: [] });
      expect(wrp.contains('.index-of-things')).toBe(true);

      wrp.setData({ people: [person1, person2]});
      expect(wrp.contains('.index-of-things')).toBe(true);
    });

    it('conditionally displays container <entity>-index-row', () => {
      // If people array is empty, the v-for has nothing to iterate
      // over and we therefore render no rows on the UI.
      wrp.setData({ people: []});
      expect(wrp.contains('.person-index-row')).toBe(false);

      // But if we have people to show, we then should see the exact number of
      // rows.
      // @TODO: (ignoring pagination for this stage of the project).
      wrp.setData({ people: [person1, person2] });
      expect(wrp.findAll('.person-index-row').length).toEqual(2);
    });

    it('each <entity>-index-row has an edit button', () => {
      wrp.setData({ people: [person1, person2] });
      const rows = wrp.findAll('.person-index-row');
      // <v-btn> renders as <a href="..."> because of :to property.
      expect(rows.contains('a.btn-edit')).toBe(true);
    });

  }); // Basic Elements


  describe('Fetch/Persist Data', () => {
    it('fetches and renders some <entities>', async () => {
      expect.assertions(4);
      await wrp.vm.fetchPeople();
      wrp.update();
      const rows = wrp.findAll('.index-of-things .person-index-row');
      expect(rows.at(0).text()).toContain('Foo');
      expect(rows.at(0).text()).toContain('foo@foo.net');
      expect(rows.at(1).text()).toContain('Bar');
      expect(rows.at(1).text()).toContain('bar@bar.net');
    });

    it('handles requests to <entities> index 404 url', async () => {
      await wrp.vm.fetchPeople('/people-404').catch(err => {
        expect(err.status).toBe(404);
        expect(err.data.error).toBe('Not Found: 404');
        store.displayAlertAction({
          type: 'warning',
          text: 'Erro a listar pessoas.',
        });
  
        expect(store.state.alert.type).toEqual('warning');
        expect(store.state.alert.text).toEqual('Erro a listar pessoas.');
        expect(store.state.alert.display).toBe(true);
      });
    });

    it('invokes fetch<entities> when component is created', () => {
      const fetchPeople = jest.fn();
      const lwrp = shallow(PersonIndex, {
        methods: { fetchPeople }
      });
      expect(fetchPeople).toHaveBeenCalled();
    });

    it('invokes fetch<entities> to a 500 sorts of serve response', async () => {
      expect.assertions(1);
      return wrp.vm.fetchPeople('/no-no-no')
        .catch(err => {
          expect(err.data.error).toEqual('Internal Server Error: 500');
        });
    });

  }); // Fetch/Persist Data

}); // PersonIndex.spec.js
