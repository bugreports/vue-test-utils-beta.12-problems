import { mount, shallow, createLocalVue } from '@vue/test-utils';
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import StatusIndex from '@cmps/StatusIndex.vue';
import StatusForm from '@cmps/StatusForm.vue';
import axios from 'axios';
import { store } from '@/store/store';

import * as H_ from '../helpers';

/** JEST MOCKS {{{1 */
// Jest mocks are hoisted to the top of the file no mater
// where you put them.
// https://facebook.github.io/jest/docs/en/manual-mocks.html#using-with-es-module-imports
jest.mock('axios', () => ({
  // path is what comes from the parameter to axios.get('/some-path').
  get: jest.fn((path) => { // {{{2
    if ('/statuses/1' === path) {
      return Promise.resolve({
        status: 200,
        statusText: 'OK',
        data: { id: 1, name: 'Foo' },
      });
    }
    else if ('/status-404' === path) {
      return Promise.resolve({
        status: 404,
        data: { error: 'Not found: 404' }
      });
    }
    else {
      return Promise.reject({ error: 'Unknown Error' });
    }
  }),

  post: jest.fn((path, statusPayload) => { // {{{2
    // POST _without_ and ID means we are creating a new resource.
    if (path === '/statuses') {
      if (0 < statusPayload.name.length) {
        return Promise.resolve({
          status: 201,
          data: { id: statusPayload.id, name: statusPayload.name },
        });
      }
      // Simulate user provided invalid or insufficient data.
      else if (0 === statusPayload.name.length) {
        return Promise.resolve({
          status: 422, // Unprocessable Entity
          data: { id: statusPayload.id, name: statusPayload.name },
        });
      }
    }
  }),

  patch: jest.fn((path, payload) => { // {{{2
    // PATCH with an ID means we are updating an existing resource.
    if (path === '/statuses/1') {
      if (0 < payload.name.length) {
        return Promise.resolve({
          status: 200,
          data: { id: payload.id, name: payload.name },
        });
      }
      else if (0 === payload.name.length) {
        return Promise.resolve({
          status: 422,
          statusText: 'Unprocessable Entity',
          data: { id: payload.id, name: payload.name },
        });
      }
    }
  }),
})); // }}}

describe('StatusForm.spec.js', function () { // {{{1

  let wrpNew;
  let wrpEdit;

  const routeIndex = { path: '/statuses', name: 'statuses' };
  const routeNew = { path: '/statuses/new', name: 'status-new' };
  const routeEdit = { path: '/statuses/:status_id/edit', name: 'status-edit' };
  const routes = [ routeIndex, routeNew, routeEdit ];
  const routerNew = new VueRouter({ mode: 'abstract', routes });
  const routerEdit = new VueRouter({ mode: 'abstract', routes });

  beforeEach(() => { // {{{2

    const localVueNew = createLocalVue();
    const localVueEdit = createLocalVue();

    localVueNew.use(VueRouter);
    localVueNew.use(Vuetify);

    localVueEdit.use(VueRouter);
    localVueEdit.use(Vuetify);

    routerNew.push({ name: 'status-new' });
    routerEdit.push({ name: 'status-edit', params: { status_id: 1 }});

    wrpNew = mount(StatusForm, {
      localVue: localVueNew,
      router: routerNew,
    });

    wrpEdit = mount(StatusForm, {
      localVue: localVueEdit,
      router: routerEdit,
    });
  }); // }}}

  describe('Default Structure', () => { // {{{2

    it('has container with proper <entity> and <entity>-form css classes', () => {
      expect(wrpNew.contains('.status.status-form')).toBe(true);
      expect(wrpEdit.contains('.status.status-form')).toBe(true);
    });

    it('has ‘isEditing’ true or false', () => {
      expect(wrpNew.vm.isEditing).toBe(false);
      expect(wrpEdit.vm.isEditing).toBe(true);
    });

    it('has ‘h2.crud-title’ with proper create/edit text', () => {
      expect(wrpNew.contains('h2.crud-title')).toBe(true);
      expect(wrpEdit.contains('h2.crud-title')).toBe(true);
      expect(wrpNew.find('h2.crud-title').text()).toEqual('Status - Adicionar');
      expect(wrpEdit.find('h2.crud-title').text()).toEqual('Status - Editar');
    });

    it('has a ‘form’ for the new/editing <entity>', () => {
      expect(wrpNew.contains('.status-form form')).toBe(true);
      expect(wrpEdit.contains('.status-form form')).toBe(true);
    });

    it('has cancel and submit buttons', () => {
      expect(wrpNew.contains('.status-form .btn-cancel')).toBe(true);
      expect(wrpNew.contains('.status-form .btn-submit')).toBe(true);
      expect(wrpEdit.contains('.status-form .btn-cancel')).toBe(true);
      expect(wrpEdit.contains('.status-form .btn-submit')).toBe(true);
    });

    it('has container with proper class plus label for each field', () => {
      const elemNew = wrpNew.find('.wrp-status-name');
      const elemEdit = wrpEdit.find('.wrp-status-name');

      expect(elemNew.find('label').text()).toEqual('Nome do Status');
      expect(elemEdit.find('label').text()).toEqual('Nome do Status');

      expect(elemNew.contains("input[name='status[name]']")).toBe(true);
      expect(elemEdit.contains("input[name='status[name]']")).toBe(true);
    });

  }); // Default Structure }}}

  describe('Form - Create/Edit', () => { // {{{2

    it('new <entity> form empty fields', () => {
      const elemNew = wrpNew.find('.wrp-status-name');
      expect(elemNew.find("input[name='status[name]']").element.value).toEqual('');
    });

    it('edit <entity> form has filled in fields', () => {
      const elemEdit = wrpEdit.find('.wrp-status-name');
      expect(elemEdit.find("input[name='status[name]']").element.value).toEqual('Foo');
    });

    it('cancels the create/edit action', () => {
      const spyNew = jest.spyOn(wrpNew.vm.$router, 'push');
      const spyEdit = jest.spyOn(wrpEdit.vm.$router, 'push');
      wrpNew.find('.status-form .btn-cancel').trigger('click');
      wrpEdit.find('.status-form .btn-cancel').trigger('click');
      const routeArgs = {
        name: 'statuses',
        params: {},
        path: '/statuses',
      };
      expect(spyNew).toHaveBeenCalledWith(routeArgs);
      expect(spyEdit).toHaveBeenCalledWith(routeArgs);
    });

    it('redirects to index if create was successfull', async () => {
      const pushSpy = jest.spyOn(wrpNew.vm.$router, 'push');
      const btnSubmit = wrpNew.find('button.btn-submit');

      typeIn(wrpNew, "input[name='status[name]']", 'New Status');
      btnSubmit.trigger('click');

      await wrpNew.vm.$nextTick();

      expect(pushSpy).toHaveBeenCalledWith({
        name: 'statuses',
        params: {},
        path: '/statuses'
      });
    });

    it('redirects to index if update was successful', async () => {
      const spyPush = jest.spyOn(wrpEdit.vm.$router, 'push');
      const btn = wrpEdit.find('button.btn-submit');

      typeIn(wrpEdit, '.wrp-status-name input', 'Edited Status');
      expect(wrpEdit.vm.status.name).toEqual('Edited Status');

      await btn.trigger('click');

      expect(spyPush).toHaveBeenCalledWith({
        name: 'statuses',
        params: {},
        path: '/statuses'
      });
      // @TODO
      // $route.name now is still _not_ 'statuses'. Not even if we
      // wait for next tick. Why not‽
    });

    it('updates store with alert message when create fails', () => {
      // Component detectes a non 200 status response and updates
      // store so the UI app it displays an alert/message box
      // telling the user about the problem.

      const spyAlert = jest.spyOn(store, 'displayAlertAction');
      const btnSubmit = wrpNew.find('.btn-submit');
      const inputName = wrpNew.find('.wrp-status-name input');

      // Name can't be blank, so it fails.
      typeIn(wrpNew, inputName, '');
      btnSubmit.trigger('click');

      return Vue.nextTick()
        .then(() => {
          expect(spyAlert).toHaveBeenCalledWith({
            type: 'warning',
            text: 'Erro ao inserir dados do status.'
          });
        });
    });

    it('updates store with alert message when create succeeds', () => {

      const spyAlert = jest.spyOn(store, 'displayAlertAction');
      const btnSubmit = wrpNew.find('.btn-submit');
      const inputName = wrpNew.find('.wrp-status-name input');

      // Name can't be blank, so it fails.
      typeIn(wrpNew, inputName, 'My New Status');
      btnSubmit.trigger('click');

      return wrpNew.vm.$nextTick()
        .then(() => {
          expect(spyAlert).toHaveBeenCalledWith({
            type: 'success',
            text: 'Dados do status inseridos com sucesso!',
          });
        });
    });

    it('updates store with alert message when update fails', () => {
      const spyAlert = jest.spyOn(store, 'displayAlertAction');
      const btnSubmit = wrpEdit.find('.btn-submit');
      const inputName = wrpEdit.find('.wrp-status-name input');

      typeIn(wrpEdit, inputName, '');
      btnSubmit.trigger('click');

      return Vue.nextTick()
        .then(() => {
          expect(spyAlert).toHaveBeenCalledWith({
            type: 'warning',
            text: 'Erro ao atulizar dados do status.'
          });
        });
    });

    it('updates store with alert message when update succeeds', () => {
      const spyAlert = jest.spyOn(store, 'displayAlertAction');
      const btnSubmit = wrpEdit.find('.btn-submit');
      const inputName = wrpEdit.find('.wrp-status-name input');

      typeIn(wrpEdit, inputName, 'Edited Status');
      btnSubmit.trigger('click');

      return Vue.nextTick()
        .then(() => {
          expect(spyAlert).toHaveBeenCalledWith({
            type: 'success',
            text: 'Dados do status atualizados com sucesso!',
          });
        });
    });

  }); // Form - Create/Edit }}}

});

