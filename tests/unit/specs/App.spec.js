import { mount, shallow, createLocalVue } from '@vue/test-utils';
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import App from '@/App.vue';
import axios from 'axios';
import { routes } from '@/routes/routes';
import { store } from '@/store/store';

//
// NOTE: Since we are not mocking the store (we are using
// the _real_ store), we are getting a new, fresh store state.
// So, we manually reset it before each test.
//

addElemWithDataAppToBody();

jest.mock('axios', () => ({
  // path is what comes from the parameter to axios.get('/some-path').
  get: jest.fn((path) => {
    if ('/user/details' === path) {
      return Promise.resolve({
        status: 200,
        statusText: 'OK',
        data: { id: 1, name: 'Test User', email: 'testuser@email.net' },
      });
    }
    else if ('/details-404' === path) {
      return Promise.resolve({
        status: 404,
        data: { error: 'Not found: 404' }
      });
    }
    else {
      return Promise.reject({ error: 'Unknown Error' });
    }
  }),
}));

describe('App', function () {

  let wrp;

  beforeEach(() => {
    const lvue = createLocalVue();
    lvue.use(Vuetify);
    lvue.use(VueRouter);

    store.resetState();

    const router = new VueRouter({
      mode: 'abstract',
      routes,
    });

    //addElemWithDataAppToBody();

    wrp = mount(App, {
      localVue: lvue,
      router,
    });
  });

  it('displays the app', () => {
    expect(wrp.contains('.application--wrap')).toBe(true);
    expect(wrp.contains('div')).toBe(true);
  });

  it.only('displays system dialog message', async () => {

    // By default, we have this. Note that `.dialog--active` is added
    // by Vuetify when there is a dialog showing.
    expect(wrp.contains('.dialog--active')).toBe(false);

    expect(wrp.find('.dialog__content .headline').text()).toEqual('Notificação do Sistema');
    expect(wrp.find('.dialog__content .text').text()).toEqual('');

    //// We then set the data and trigger the displaying of the modal.
    wrp.vm.store.setSysmsgHeadingAction('New Headline');
    wrp.vm.store.setSysmsgTextAction('Please use the force.');
    wrp.vm.store.setSysmsgDisplayAction(true);

    // And we should see things working as expected.
    return Vue.nextTick()
      .then(() => {
        expect(wrp.contains('.dialog--active')).toBe(true);
        expect(wrp.find('.dialog__content .headline').text()).toEqual('New Headline');
        expect(wrp.find('.dialog__content .text').text()).toEqual('Please use the force.');
      }).catch(err => { });

    //// Let's also see we can close the modal.
    //wrp.find('.js-sysmsg .js-btn-close').trigger('click');
    //Vue.nextTick()
    //  .then(() => {
    //    expect(wrp.contains('.dialog--active')).toBe(false);
    //  });
  });

  it('can display success alert', () => {

    const alertWrp = wrp.find('.alert.alert--dismissible');
    // Alert component is there, but not being displayed.
    expect(wrp.contains('.alert.alert--dismissible')).toBe(true);
    expect(alertWrp.element.style.display).toEqual('none');

    // Now we tell the store that there is a message to display.
    store.displayAlertAction({
      text: 'The force is strong with this one.',
      type: 'success',
    });

    // And then it should be visible.
    Vue.nextTick()
      .then(() => {
        // Display is '' (which means it is block by default). It is
        // the way this Vuetify's component works. It toggles display
        // 'none' and ''.
        expect(alertWrp.element.style.display).toEqual('');
        expect(alertWrp.find('.text').text()).toEqual('The force is strong with this one.');
        // `visible()' added in 1.0.0-beta.11
        expect(alertWrp.visible()).toBe(true);
        expect(alertWrp.classes()).toContain('success');
      });
  });

  it('can display error alert', () => {

    const alertWrp = wrp.find('.alert.alert--dismissible');
    // Alert component is there, but not being displayed.
    expect(wrp.contains('.alert.alert--dismissible')).toBe(true);
    expect(alertWrp.element.style.display).toEqual('none');

    // Now we tell the store that there is a message to display.
    store.displayAlertAction({
      text: 'I cannot teach him. The boy has no patience.',
      type: 'error',
    });

    // And then it should be visible.
    Vue.nextTick()
      .then(() => {
        // Display is '' (which means it is block by default). It is
        // the way this Vuetify's component works. It toggles display
        // 'none' and ''.
        expect(alertWrp.element.style.display).toEqual('');
        expect(alertWrp.find('.text').text()).toEqual('I cannot teach him. The boy has no patience.');
        // `visible()' added in 1.0.0-beta.11
        expect(alertWrp.visible()).toBe(true);
        expect(alertWrp.classes()).toContain('error');
      });
  });
});

